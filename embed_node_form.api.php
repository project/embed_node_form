<?php
/**
 * @file
 *   Hook documentation for the "Embedded Node Form API".
 *
 *   © 2015-2016 House at Work. All rights reserved.
 *
 * @author Guy Paddock (guy@redbottledesign.com)
 */

/**
 * A hook for modifying what information is submitted to an embedded node form.
 *
 * @param array $context
 *   An array containing the context of what's about to be submitted, based on
 *   the arguments passed to <code>embed_node_form_submit_form()</code>. The
 *   keys are:
 *     - parent_form_state: A reference to a copy of the form state of the
 *       parent form.
 *     - field_whitelist: A reference to an array of machine names of all field
 *       that were embedded from the node form. Only information for these
 *       fields is automatically copied into the form state when submitting the
 *       embedded node form.
 *     - field_defaults: A reference to an associative array that provides
 *       default values for any node fields that were not included in the
 *       embedded form but that should still be populated (for example, node
 *       title, author, publishing status, etc). The keys are the attribute
 *       names, and the values are the default values.
 *     - extra_form_state_keys: An optional list of additional keys to copy from
 *       the parent form state directly into the embedded form state.
 */
function hook_embedded_node_form_submit_alter(array &$context) {
  $parent_form_state      =& $context['parent_form_state'];
  $field_whitelist        =& $context['field_whitelist'];
  $field_defaults         =& $context['field_defaults'];
  $extra_form_state_keys  =& $context['extra_form_state_keys'];
  
  // Copy over some other values based on the parent form state
  if (!empty($parent_form_state['values']['want_extra'])) {
    $field_whitelist[] = 'extra_metadata';
  }
  else {
    // Provide a default value for the field.
    $field_defaults['extra_metadata'] = t('No value provided');
  }

  // Copy over Node API "field" metadata.
  $extra_form_state_keys[] = 'field';
}

/**
 * A hook to define mappings from widget types to form state fix-up callbacks.
 *
 * This hook is expected to return an associative array. Each key in the map is
 * a widget type, and the value is an array of callbacks, which are each passed
 * the following arguments when that form element is encountered during
 * submission of an embedded form:
 *   - the form element (as an array)
 *   - a reference to the form state
 *
 * When multiple modules define callbacks for the same widget type, all of the
 * defined callbacks are invoked.
 * 
 * @return array<string, array<string>>
 *   The map of callbacks defined by this module. Each key is a widget type
 *   (like "select" or "managed_file"), and the value is an array of callbacks.
 */
function hook_embedded_node_field_fixup_map() {
  $fixup_map = array(
    'select' => array('embed_node_form_fixup_nested_value_field'),
  );

  return $fixup_map;
}

/**
 * A hook for altering the map from field types to form state fix-up callbacks.
 *
 * Each key in the map is a field type, and the value is an array of callbacks,
 * which are each passed the following arguments when that form element is
 * encountered during submission of an embedded form:
 *   - the form element (as an array)
 *   - a reference to the form state
 *
 * @param array<string, array<string>>
 *   The map to alter.
 */
function hook_embedded_node_field_fixup_map_alter(array &$fixup_map) {
  // Add a new callback to the end of the callback chain
  $fixup_map['select'][] = array('custom_select_fixup');
}