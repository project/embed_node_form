# Embedded Node Form API
By Guy Paddock ([guy@redbottledesign.com](mailto:guy@redbottledesign.com))

## Supporting people & organizations
- **[House at Work](http://houseatwork.com):** the client who commissioned the project.

## What is this?
Have you ever needed to stitch together several node forms into one giant form, perhaps as part of a multistep form workflow?

Look no further! This module provides an API for:
1. Obtaining the form elements for creating a node of the desired content type (in a way that the form elements can still be altered)
2. Stitching the elements into a larger form that can be rendered like a normal custom form
3. Processing / fixing-up the data collected through the larger form at the appropriate times in its workflow
4. Submitting the data programmatically through the proper node forms so that the nodes are constructed as expected

## Why do I need a module for this?
Prior to this module being in existence, there were several approaches you could take:
1. **You could build a custom form out of standard FAPI form elements and avoid using Field API widgets.** You could then programmatically submit each of the node forms when the whole form is submitted. Unfortunately, this makes it difficult to accept content that's compatible with the nodes -- attempting to show a file upload widget is notoriously difficult with this approach.
2. **You could attempt to render each node form one at a time on the page**, altering it as its displayed to only show the elements you want to show. You could then wire up the submit logic for each form so that it creates the appropriate node and then displays the next form. Unfortunately, this makes it difficult to do an "all or nothing" style workflow where everything gets created at once, and could lead to a very inflexible workflow.
3. **You could attempt to have the FAPI render each of the node forms into render arrays, then stitch them together into a larger form that is able to use Field API widgets.** The custom form could then submit each of the node forms when the whole form is submitted, in the format that each node form expects. Depending on the fields and widgets involved, this could sometimes lead to the form rendering strangely, but is ideal for making flexible workflows.
4. **You could write a module that defines a new type of entity that is a composite of the fields from the other nodes you want**, and then have its create / edit forms collect values and submit them into forms for the underlying node types. This is tedious to write, but would leverage Drupal's standard Entity / Field API form rendering to present the forms, allowing widgets to render consistently.

**The current 1.x version is an implementation of approach #3 above. We'd envision a 2.x version at some point that might implement approach #4, but the current solution is workable.**

## How to Get a Node Form for Embedding
Here's one possible way to embed the node form for a `project` content type in a custom form:
```PHP
function my_form(array $form, array &$form_state) {
  // See `my_form_project_whitelist()` defined below
  $field_whitelist = my_form_project_whitelist();
  
  // This call actually fetches the appropriate form for the 'project' content
  // type, filters out fields that aren't on the whitelist, and sets up storage
  // within the parent form for the form state of the embedded form.
  $project_form =
    embed_node_form_get_form(
      'project',
      $form_state,
      $field_whitelist,
       // 'field' is commonly copied over to support Field API widgets
      array('field'));

  // You can wrap the resulting "form" element any way you'd like in the
  // overall form. The form elements inside can also be altered like any regular
  // form elements, provided you don't break any Field API-specific metadata
  // that's stored in attributes on the fields.  
  $project_form['#type'] = 'item';
  $form['project']['form'] = $project_form;

  // This is only one possible way to get code that executes when the parent
  // form is submitted. You could also just define `my_form_submit`, which
  // Drupal will implicitly call, and put the submission logic in there.
  $form['#submit'][] = 'my_form_project_submit';
}

function my_form_project_whitelist() {
  // Each of the field names in the whitelist are attributes or fields within
  // the node type. Any fields that don't appear in this list are excluded from
  // the form that's returned. If no whitelist is provided, all fields appear.
  $field_whitelist = array(
    'title',
    'body',
    'field_name',
    'field_pictures',
  );
  
  return $field_whitelist;
}
```
## How to Submit an Embedded Node Form
You can submit the embedded node form at any time when the parent form is being submitted (i.e. in a `#submit` callback, or the submit callback for the whole form, etc.). Ideally, you should *NOT* submit the embedded form during validation of the parent form because the form might end up with validation errors that prevent submission, and you'll end up with duplicate nodes.

Here's one possible way to submit the embedded node form that was created in the earlier example:
```PHP
function my_form_project_submit(array $form, array &$form_state) {
  // You can create a database transaction before submitting the embedded
  // node form(s) to ensure that either all of the data is created, or none of
  // it is. This keeps your data consistent.
  $transaction = db_transaction();
  
  try {
    // See `my_form_save_new_embedded_node()` defined below
    $project_node = my_form_save_new_embedded_node($form_state);
    // At this point you can do whatever you need to do with the new
    // project node.
  }
  catch (Exception $ex) {
    // Ensure that nothing gets created
    $transaction->rollback();

    // Be sure to record the error AFTER the DB rollback; otherwise, it will
    // also get rolled back
    watchdog_exception('my_form', $ex);
  }
}

function my_form_save_new_embedded_node(array &$form_state) {
  $field_defaults = array(
    'options' => array(
      'status'  => NODE_PUBLISHED,
      'promote' => NODE_NOT_PROMOTED,
      'sticky'  => NODE_NOT_STICKY,
    ),
  );

  $field_whitelist = my_form_project_whitelist();

  $project_node = embed_node_form_submit_form(
    'project',
    $form_state,
    $field_whitelist,
    $field_defaults);

  return $project_node;
}
```

## "Fixing-up" Form State Values
Sometimes, parts of Drupal's Field API doesn't behave itself. For example, option lists tends to rearrange their values during validation. This results in the values in the form state coming from the parent form not being compatible with what needs to be passed-in to the embedded node form. Basically, the Field API rearranges the content to its liking in the parent form state when the parent form is validated.

See `embed_node_form.api.php` for documentation and examples of hooks that you can implement if you need to provide your own "fix-ups" for this situation.