<?php
/**
 * @file
 *   API and fix-ups for certain field types, so that they are compatible with
 *   the Embedded Node Form API.
 *
 *   © 2015-2016 House at Work. All rights reserved.
 *
 * @author Guy Paddock (guy@redbottledesign.com)
 */

/**
 * Implements hook_embedded_node_field_fixup_map().
 */
function embed_node_form_embedded_node_field_fixup_map() {
  $fixup_map = array(
    // FAPI Element Type => Chain of Fixups
    'select' => array('embed_node_form_fixup_nested_value_field'),
    'radios' => array('embed_node_form_fixup_nested_value_field'),
  );

  return $fixup_map;
}

/**
 * Implements hook_embedded_node_form_submit_alter().
 *
 * Applies the default form state fix-ups that this module ships with, to allow
 * known troublesome field types to validate and submit successfully in each
 * embedded form.
 */
function embed_node_form_embedded_node_form_submit_alter(array &$context) {
  $fixup_map = embed_node_form_get_field_fixup_map();

  $form_state =& $context['parent_form_state'];
  $form       = $form_state['complete form'];

  $field_whitelist = $context['field_whitelist'];

  _embed_node_form_apply_element_fixups(
    $form, $form_state, $fixup_map, $field_whitelist);
}

/**
 * Gets the map of field types to form state fix-up callbacks.
 *
 * Each key in the map is a field type, and the value is an array of
 * callbacks, which are each passed:
 *   - the form element (as an array)
 *   - a reference to the form state
 *
 * Additional fix-ups can be defined by implementing
 * <code>hook_embedded_node_fixup_map()</code>. The map can be altered before it
 * is returned by implementing
 * <code>hook_embedded_node_fixup_map_alter()</code>.
 *
 * @return array
 */
function embed_node_form_get_field_fixup_map() {
  $fixup_map = module_invoke_all('embedded_node_field_fixup_map');

  drupal_alter('embedded_node_field_fixup_map', $fixup_map);

  return $fixup_map;
}

/**
 * Fix-up for fields that re-arrange values into nested arrays during validation.
 *
 * For example:
 *  - Taxonomy terms
 *  - Select lists
 *  - Node reference fields
 *
 * @param array $element
 *   The form element, as it appears in the form.
 * @param array $form_state
 *   A reference to the form state, which will be adjusted appropriately for
 *   this type of form element.
 */
function embed_node_form_fixup_nested_value_field(array $element, array &$form_state) {
  if (!empty($element['#value_key'])) {
    $value_key  = $element['#value_key'];
    $parents    = $element['#parents'];
    $value_path = array_merge(array('values'), $parents);

    $source_values = drupal_array_get_nested_value($form_state, $value_path);

    if (!empty($source_values)) {
      $remapped_values = array();

      if (is_array($source_values)) {
        foreach ($source_values as $key => $value) {
          if (isset($value[$value_key])) {
            $remapped_values[$key] = $value[$value_key];
          }
        }
      }

      if (!empty($remapped_values)) {
        drupal_array_set_nested_value(
          $form_state, $value_path, $remapped_values);
      }
    }
  }
}

/**
 * Applies all applicable fix-ups to the form state for the specified element.
 *
 * @param array $element
 *   The form element, as an array, the way it appears in the form.
 * @param array $form_state
 *   A reference to the form state of the form.
 * @param array $fixup_map
 *   The pre-fetched map of fix-up callbacks.
 * @param array $field_whitelist
 *   The list of machine names for fields that are eligible for fix-ups.
 */
function _embed_node_form_apply_element_fixups(array $element,
                                               array &$form_state,
                                               array $fixup_map,
                                               array $field_whitelist) {
  if (!empty($element['#field_name']) && !empty($element['#type'])) {
    $name = $element['#field_name'];
    $type = $element['#type'];

    if (in_array($name, $field_whitelist) && isset($fixup_map[$type])) {
      $fixups = $fixup_map[$type];

      foreach ($fixups as $fixup) {
        $fixup($element, $form_state);
      }
    }
  }

  foreach (element_children($element) as $child_key) {
    $child_element = $element[$child_key];

    _embed_node_form_apply_element_fixups(
      $child_element, $form_state, $fixup_map, $field_whitelist);
  }
}
