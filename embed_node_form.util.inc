<?php
/**
 * @file
 *   Various utility functions for the "Embedded Node Form API".
 *
 *   © 2015-2016 House at Work. All rights reserved.
 *
 * @author Guy Paddock (guy@redbottledesign.com)
 */

/**
 * Filters out all keys from the provided array not on the provided whitelist.
 *
 * @param array $array
 *   The array to filter.
 * @param array $whitelist
 *   The names of the keys to preserve.
 *
 * @return array
 *   The array, with only white-listed elements preserved.
 */
function embed_node_form_whitelist_fields(array $array, array $whitelist) {
  return array_intersect_key($array, array_flip($whitelist));
}

/**
 * Copies certain values from one form state to another.
 *
 * @param array $source_form_state
 *   The form state from which values will be taken.
 * @param array $target_form_state
 *   The form state to which values will be written.
 * @param array $keys
 *   The list of keys to copy from one form state to the other.
 */
function embed_node_form_copy_form_state_values(array &$source_form_state,
                                                array &$target_form_state,
                                                array $keys) {
  foreach ($keys as $key) {
    if (isset($source_form_state[$key])) {
      if (isset($target_form_state[$key]) &&
          is_array($target_form_state[$key])) {
        $target_form_state[$key] += $source_form_state[$key];
      }
      else {
        $target_form_state[$key] = $source_form_state[$key];
      }
    }
  }
}

/**
 * Gets the name of the form for the specified node type.
 *
 * @param string $node_type
 *   The machine name of the content type.
 *
 * @return string
 *   The form ID for the node form for the specified content type.
 */
function embed_node_form_get_form_id($node_type) {
  return $node_type . '_node_form';
}